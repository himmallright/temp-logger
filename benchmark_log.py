from psutil import sensors_temperatures
from time import sleep

SAMPLE_RATE=2 ## In sec
DELIM= ","

#OUTPUT_DIR="/home/ryan/Documents/benchmarks/"
OUTPUT_DIR=""

DATA_PAIRS= [
    ('cpu_temp', '["k10temp"][0][1]'),
    ('gpu_edge', '["amdgpu"][0][1]'),
    ('nvme_composite', '["nvme"][0][1]'),
               ]

def create_log_dir():
    """Creates the log files.."""


def get_data():
    """Calls the functions to get sensor data."""
    return sensors_temperatures()


def log_data(timestamp, data_pairs, data):
    """Logs the data to all the files."""
    for pair in data_pairs:
        data_value = eval('data' + pair[1])
        log_line(timestamp, pair[0], data_value)


def log_line(timestamp, data_name, data_value, delim=DELIM):
    """Logs a line of data to a file."""
    filename= OUTPUT_DIR + data_name + '.log'
    with open(filename, "a+") as datalog:
        datalog.write(str(data_value) + '\n')


def logging_loop(sample_rate, data_pairs):
    """THe main loop that calls the command and writes the logs."""
    while True:
        sleep(sample_rate)
        data = get_data()
        timestamp = ""
        log_data(timestamp, data_pairs, data)


# Exec main loop
logging_loop(SAMPLE_RATE, DATA_PAIRS)
